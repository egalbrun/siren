#!/usr/bin/env python

import wx
import multiprocessing
import time
import sys

# import warnings
# warnings.simplefilter("ignore")

from blocks.mine.classData import Data
from blocks.interface.classSiren import Siren
from blocks.interface.classGridTable import CustRenderer
from blocks.mine.classPreferencesManager import getPreferencesReader
from blocks.mine.classPackage import IOTools

import pdb
import numpy
## MAIN APP CLASS ###


class SirenApp(wx.App):
    def __init__(self, *args, **kwargs):
        wx.App.__init__(self, *args, **kwargs)
        # Catches events when the app is asked to activate by some other process
        self.Bind(wx.EVT_ACTIVATE_APP, self.OnActivate)

    def OnInit(self):
        # Set the app name here to *hard coded* Siren
        self.SetAppName("Siren")
        self.frame = Siren()
        series = ""
        import sys
        import os
        import platform
        import re

        params = {}
        if len(sys.argv) > 1 and platform.system() != 'Darwin':
            # On OSX, MacOpenFile() gets called with sys.argv's contents, so don't load anything here
            # print("Loading file", sys.argv[-1])
            preferences_reader = self.frame.dw.getPreferencesReader()
            params, leftover_args, preferences_mod = preferences_reader.getPreferences(sys.argv)
            self.frame.dw.updatePreferences(params)
            
            if "src_folder" in params.get("filename", {}):
                src_folder = params["filename"]["src_folder"]
            else:
                src_folder = os.path.dirname(os.path.abspath(__file__))
                
            pack_filename = None
            if "pack_file" in params.get("filename", {}):
                pack_filename = params["filename"]["pack_file"][0]
                try:
                    self.frame.dw.openPackage(pack_filename)
                except Exception:
                    del params["filename"]["pack_file"]
                for k,v in self.frame.dw.getPreferences().items():
                    if k not in preferences_mod:
                        params[k] = v                    

                    
            task_load = {"queries_basic_dest":"in", "with_log": False, "log": False}
            loaded = IOTools.loadUnpackaged(params, src_folder=src_folder,
                                            pack_filename=pack_filename, tmp_dir=self.frame.dw.getPackTmpDir(), task_load=task_load,
                                            data=self.frame.dw.getData(), reds=self.frame.dw.getCountLoadedRedLists())
            
            if self.frame.dw.getData() is None and loaded.get("data") is not None:
                self.frame.dw.setData(loaded.get("data"))
                self.frame.dw.applyVarsMask(params)
                self.frame.dw._isChanged = True
                self.frame.dw._isFromPackage = False

            if loaded.get("reds") is not None:
                for rlist in loaded["reds"]:
                    self.frame.dw.appendRedsToSrc(rlist["items"], rlist["src"], recompute=False, set_changed=False)
                    self.frame.dw.incrementCountLoadedRedLists()
                
        self.frame.refresh()

        if params.get("debug", False):
            # self.frame.OnPreferencesDialog(None)
            # self.frame.OnExtensionsDialog(None)
            ## self.frame.OnFoldsDialog(None)
            # self.frame.OnConnectionDialog(None)
            # print "No debug action..."
            # print("Loading file", sys.argv[-1])
            # self.frame.expand()

            # self.frame.OnRunTest(None)

            # ### SPLITS
            # self.frame.dw.getData().extractFolds(1, 12)
            # splits_info = self.frame.dw.getData().getFoldsInfo()
            # stored_splits_ids = sorted(splits_info["split_ids"].keys(), key=lambda x: splits_info["split_ids"][x])
            # ids = {}
            # checked = [("learn", range(1,len(stored_splits_ids))), ("test", [0])]
            # for lt, bids in checked:
            #     ids[lt] = [stored_splits_ids[bid] for bid in bids]
            # self.frame.dw.getData().assignLT(ids["learn"], ids["test"])
            # self.frame.recomputeAll()

            # fmts = ["tiff"] #, "png"] #, "eps"]
            fmts = ["eps"] #, "eps"]
            fmts = ["svg", "eps"]
            fmts = ["png"]
            # (1641, 670), (1064, 744), (551, 375)
            # tab, fname, dims = ("reds", "/home/egalbrun/R%d_map_2K-d100.", (1920, 1190)) ### MAP RED
            # tab, fname, dims = ("vars", "/home/egalbrun/V%d-%d_map_2K-d100.", (2350, 1190)) ### MAP VAR
            folder = "/home/egalbrun/figs"
            # if len(series) > 0:
            #     folder += "/"+series
            # tab, fname, dims = ("reds", folder+"/R%d_%s_2K-d100.", (1920, 1190)) ### MAP RED
            #tab, fname, dims = ("vars", folder+"/V%d-%d_map_2K-d100.", (2500, 1190)) ### MAP VAR
            tab = "reds"
            dims = (800, 1000)
            fo = None
            if not os.path.exists(folder):
                os.mkdir(folder)
            for li, lid in enumerate(self.frame.tabs[tab]["tab"].getOrdLids()):
                collect_vals = []
                lname = self.frame.tabs[tab]["tab"].getListData(lid=lid)["name"].split("/")[-1].split(".")[0]
                if li == 0 and len(self.frame.tabs[tab]["tab"].getOrdLids()) > 7:
                    fo = open("/home/egalbrun/tmp_%s.txt" % lname.split("-")[-1], "w")
                    lbls = ["base_name", "li", "ri", "acc", "lenI", "lenU", "lenLHS", "orLHS", "andLHS", "lenRHS", "orRHS", "orRHS", "lenBoth", "orBoth", "andBoth"]
                    fo.write("\t".join(lbls)+"\n")

                for ri, (iid, item) in enumerate(self.frame.tabs[tab]["tab"].getItemsList(lid=lid)):
                    basis_name = "%d_%d_%s_%s" % (10**3 * item.getAcc(), ri, lname, iid)

                    if fo is not None:
                        vals = [li, ri, 10**3 * item.getAcc(),
                            item.getLenI(), item.getLenU(),
                            len(item.query(0)), item.query(0).usesOr(), item.query(0).usesAnd(),
                            len(item.query(1)), item.query(1).usesOr(), item.query(0).usesAnd(),
                            len(item.query(0))+len(item.query(1)), item.query(0).usesOr() or item.query(1).usesOr(), item.query(0).usesAnd() or item.query(1).usesAnd()]
                        collect_vals.append(vals)
                        fo.write("\t".join([basis_name] + ["%d" % v for v in vals])+"\n")

                    else:
                        mapV = self.frame.viewOpen(item, iid=iid, viewT="MAP")
                        mapV.lastStepInit(blocking=True)
                        mapV.getLayH().getFrame().SetClientSize(dims[0], dims[1])
                        mapV.addStamp("MAP", force=True)
                        for fmt in fmts:
                            mapV.getLayH().savefig("%s/%s.%s" % (folder, basis_name, fmt), format=fmt)
                        mapV.getLayH().OnKil()
                if fo is not None and len(collect_vals) > 0:
                    XX = numpy.array(collect_vals)
                    fo.write("\t".join(["LIST_"+lname] + ["%d" % li, "%d" % XX.shape[0]] + ["%.4f" % v for v in XX[:,2:].mean(axis=0)])+"\n")
            if fo is not None:
                fo.close()
            sys.exit()
            # iid = 1
            # # self.frame.viewOpen(self.frame.getRed(iid), iid=iid, viewT="AXE_entities") #"CLM")
            # iid = (1, 33)
            # # self.frame.viewOpen(self.frame.getData().getItem(iid), iid=iid, viewT="AXE_entities")
            # self.frame.viewOpen(self.frame.getData().getItem(iid), iid=iid, viewT="MAP")

            # iids = self.frame.getData().getIidsList((0,0))
            # what = [(iid, self.frame.getData().getItem(iid)) for iid in iids]
            # # iids = self.frame.getRedLists().getIidsList(3)
            # # what = [(iid, self.frame.getRedLists().getItem(iid)) for iid in iids]
            # # self.frame.viewOpen(what, iid=-1, viewT="LRNG")

            # tab ="vars"
            # self.frame.dw.getData().getMatrix()
            # self.frame.dw.getData().selected_rows = set(range(400))
            # for i in [5]: #range(4):
            #     self.frame.tabs[tab]["tab"].viewData(i, "TR")
            # vw = self.frame.tabs[tab]["tab"].viewData((0,9), "MAP")
            #vw.updateRSets({'rset_id': 'test'})

        return True

    def BringWindowToFront(self):
        try:
            pass
            # self.frame.toolFrame.Raise()
        except:
            pass

    def OnActivate(self, event):
        pass
        # if event.GetActive():
        #     self.BringWindowToFront()
        # event.Skip()

    def MacOpenFiles(self, filenames):
        """Called for files dropped on dock icon, or opened via Finder's context menu"""
        import sys
        import os.path
        filename = filenames[0]
        # When start from command line, this gets called with the script file's name
        if filename != sys.argv[0]:
            if self.frame.dw.getData() is not None:
                if not self.frame.checkAndProceedWithUnsavedChanges():
                    return
            (p, ext) = os.path.splitext(filename)
            if ext == '.siren':
                self.frame.LoadFile(filename)
            elif ext == '.csv':

                self.frame.dw.importDataFromCSVFiles([filename, filename]+IOTools.getDataAddInfo())
                self.frame.refresh()
            else:
                wx.MessageDialog(self.frame.toolFrame, 'Unknown file type "'+ext+'" in file '+filename, style=wx.OK, caption='Unknown file type').ShowModal()

    def MacReopenApp(self):
        """Called when the doc icon is clicked, and ???"""
        self.BringWindowToFront()

    def MacNewFile(self):
        pass

    def MacPrintFile(self, filepath):
        pass


def siren_run():
    app = SirenApp(False)

    CustRenderer.BACKGROUND_SELECTED = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)
    CustRenderer.TEXT_SELECTED = wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT)
    CustRenderer.BACKGROUND = wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
    CustRenderer.TEXT = wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWTEXT)

    #app.frame = Siren()
    app.MainLoop()


def main():
    multiprocessing.freeze_support()
    siren_run()


if __name__ == '__main__':
    main()
